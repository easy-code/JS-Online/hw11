// 1. Создать функцию, которая возвращает промис. Функция принимает два рагумента - время, через которое промис должен
//    выполниться, и значение, с которым промис будет выполнен

function promiseCreator(time, val) {
    return new Promise((resolve) => {
        setTimeout(() => resolve(val), time)
    });
}

const prom = promiseCreator(500, 'Ok!');

prom.then(console.log); // Ok!


// 4. Используя fetch метод, создать get запрос к адресу https://jsonplaceholder.typicode.com/posts.
//    Отобразить в списке ul полученные поля из response/ Показать только id и title поля
const ul = document.getElementById('list');

fetch('https://jsonplaceholder.typicode.com/posts', {method: 'GET'})
    .then(response => {
        return response.json()
    })
    .then(json => {
        json.forEach(item => {
            ul.insertAdjacentHTML('beforebegin', `<li class="list-group-item">${item.id} ${item.title}</li>`);

        });
    })
    .catch(err => console.log(err));


// 5. Выполнить два запроса:
//    - https://jsonplaceholder.typicode.com/posts
//    - https://jsonplaceholder.typicode.com/users
//    Вывести в консоль информацию о количестве постов и юзеров.
//    Запросы должны выполняться одновременно, информацию выводить только после того, как будут обработаны оба запроса.
//    По желанию можно использовать встроенные инструменты js (Promise, fetch) или jQuery методы.
let totalData = {
    'posts': {},
    'users': {}
};

let postsFetch = fetch('https://jsonplaceholder.typicode.com/posts', {method: 'GET'})
    .then(res => res.json());

let usersFetch = fetch('https://jsonplaceholder.typicode.com/users', {method: 'GET'})
    .then(res => res.json());

Promise.all([postsFetch, usersFetch])
    .then(data => {
        totalData['posts'] = data[0];
        totalData['users'] = data[1];

        return totalData
    })
    .then(data => {
        console.log(`Количество постов: ${data.posts.length}`); // Количество постов: 100
        console.log(`Количество юзеров: ${data.users.length}`); // Количество юзеров: 10
    })
    .catch(err => console.log(err));
